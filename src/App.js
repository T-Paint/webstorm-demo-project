import React, {Component} from 'react';
import './App.css';
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import MainRoute from "./routes/MainRoute";
import AppHeader from "./structures/AppHeader";

class App extends Component {
    render() {
        return (
            <MuiThemeProvider>
                <div>
                    <AppHeader />
                    <MainRoute/>
                </div>
            </MuiThemeProvider>
        );
    }
}

export default App;

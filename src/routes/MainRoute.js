import React, {Component} from 'react';
import './Main.css';
import {Route, Switch} from "react-router-dom";
import MainView from "./MainView";
import MapView from "./MapView";

class Main extends Component {
    render() {
        return (
            <main>
                <Switch>
                    <Route exact path='/' component={MainView} />
                    <Route exact path='/maps' component={MapView} />
                </Switch>
            </main>
        );
    }
}

export default Main;
